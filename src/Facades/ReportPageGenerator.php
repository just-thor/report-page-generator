<?php 
namespace TFC\JasperReportManager\Facades;


use Illuminate\Support\Facades\Facade;


class ReportPageGenerator extends Facade{
    protected static function getFacadeAccessor() 
    { 
      return 'report-page-generator'; 
    }
}