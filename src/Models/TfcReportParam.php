<?php

namespace TFC\JasperReportManager\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TfcReportParam extends Model
{
  protected $table = 'tfc_report_params';
  protected $fillable = ['tfc_report_id', 'name', 'default_value', 'type', 'description'];
  public $timestamps = false;
  public function report()
  {
    return $this->belongsTo('TFC\JasperReportManager\Models\TfcReport');
  }

  public function getNameAttribute($name)
  {
    return strtolower($name);
  }
}