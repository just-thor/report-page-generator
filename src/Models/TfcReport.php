<?php

namespace TFC\JasperReportManager\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TfcReport extends Model
{
  protected $table = 'tfc_reports';
  protected $fillable = ['name', 'file_name', 'status', 'modified_date'];
  
  public function reportParams()
  {
    return $this->hasMany('TFC\JasperReportManager\Models\TfcReportParam', 'tfc_report_id', 'id');
  }
}