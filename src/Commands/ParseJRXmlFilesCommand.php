<?php

namespace TFC\JasperReportManager\Commands;

use App\User;
use App\DripEmailer;
use DB;
use Illuminate\Console\Command;
use PHPJasper\PHPJasper;
use TFC\JasperReportManager\Models\{
  TfcReport,
  TfcReportParam
};

class ParseJRXmlFilesCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'tfc:process-jrxml';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Parse and process JRXML files';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
      parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $files = glob(config('tfc-report.path') . '/*.jrxml');
    
    DB::transaction(function() use ($files) {
      //$jasper = new PHPJasper();
      TfcReportParam::truncate();

      foreach($files as $file) {
        $fileModifiedDate = date('Y-m-d H:i:s', filemtime($file));
        $fileParts = explode('/', $file);
        $filename = pathinfo(end($fileParts), PATHINFO_FILENAME);
        $name = str_replace('_', ' ', ucwords($filename, '_'));

        $tfcReport = TfcReport::firstOrNew(
          ['name' => $name ],
          [
            'file_name' => $file,
            'status' => 'Active',
            'modified_date' => $fileModifiedDate
          ]
        );
        if(empty($tfcReport->id)){
          $tfcReport->save();
        }else{
          if($tfcReport->modified_date !== $fileModifiedDate)
          {
            $tfcReport->modified_date = $fileModifiedDate;
            $tfcReport->updated_at = date('Y-m-d H:i:s');
            $tfcReport->update();
          }
        }

        $xml = simplexml_load_file($file);

        foreach($xml->parameter as $parameter) {
          $param = new TfcReportParam([
            'name' => (string)$parameter['name'], 
            'default_value' => (string)$parameter->defaultValueExpression, 
            'type' => (string)$parameter['class'],
            'description' => (string)$parameter->parameterDescription
          ]);
          
          if($param->type === 'java.lang.String' && 
            mb_strlen($param->default_value) > 0
            ) 
          {
            if($param->default_value[0] === '"') {
              $param->default_value = substr($param->default_value, 1);
            }
            $length = mb_strlen($param->default_value);
            if($param->default_value[$length - 1] === '"') {
              $param->default_value = substr($param->default_value, 0, -1);
            }
          }
          $tfcReport->reportParams()->save($param);
        }

        //$jasper->compile($file)->execute();
      }
    });
  }
}