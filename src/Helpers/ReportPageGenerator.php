<?php
namespace TFC\JasperReportManager\Helpers;

use TFC\JasperReportManager\Models\{
  TfcReport,
  TfcReportParam
};

class ReportPageGenerator {
  public function getReportList(int $size=15, Array $search=[])
  {
    return TfcReport::select('*')
      ->when(!empty($search['report_name']), function($query) use($search){
        return $query->where('name', 'ILIKE', '%' . trim($search['report_name']) . '%');
      })
      ->orderBy('name', 'asc')
      ->paginate($size);
  }

  public function getReport($reportId)
  {
    return TfcReport::select('*')->find($reportId);
  }

  public function getReportParams($reportId) 
  {
    return TfcReportParam::where('tfc_report_id', $reportId)
      ->select('id','name','default_value','description', 'type')
      ->orderBy('name', 'asc')
      ->get();
  }
}