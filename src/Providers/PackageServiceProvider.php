<?php

namespace TFC\JasperReportManager\Providers;

use Illuminate\Support\ServiceProvider;
use TFC\JasperReportManager\Commands\ParseJRXmlFilesCommand;
use App;

class PackageServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     */
    public function register()
    {
        //
    
        $this->mergeConfigFrom(
            __DIR__.'/../../config/tfc-report.php',
            'tfc-report'
        );

        App::bind('report-page-generator', function()
        {
            return new \TFC\JasperReportManager\Helpers\ReportPageGenerator();
        });
    }

    /**
     * Perform post-registration booting of services.
     */
    public function boot()
    {
        //
    
        $this->publishes([
            __DIR__.'/../../config/tfc-report.php' => config_path('tfc-report.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../../migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');
        
        if ($this->app->runningInConsole()) {
            $this->commands([
                ParseJRXmlFilesCommand::class
            ]);
        }
    }
}
