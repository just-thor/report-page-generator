<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Report Path
    |--------------------------------------------------------------------------
    |
    | This value is the path where you store the Jasper Report JRXML files.
    */

    'path' => 'resources/reports',
];