<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTfcReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tfc_reports', function (Blueprint $table) {
            $table->string('status', 10)->default('Active')->after('file_name');
            $table->timestamp('modified_date')->nullable()->after('active');
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tfc_reports', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('modified_date');
        });
    }
}
