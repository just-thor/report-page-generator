<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportParamTable extends Migration
{
    public function up()
    {
      Schema::create('tfc_reports', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('file_name');
        $table->timestamps();
      });

      Schema::create('tfc_report_params', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('tfc_report_id');
        $table->string('name');
        $table->text('default_value');
        $table->string('type');
        $table->string('description');

        $table->foreign('tfc_report_id')
            ->references('id')
            ->on('tfc_reports')
            ->onDelete('CASCADE');
      });
    }

    public function down()
    {
      Schema::drop('tfc_report_params');
      Schema::drop('tfc_reports');
    }
}